#include <iostream>
#include <ctime>
#include <cstdlib>
#include <set>
#include <vector>
#include <fstream>
#include <stdlib.h>
//#include <random>

using namespace std;

#define SIZE 20 
//#define K 6
int K = 0;



//Fraction 
class Fraction {
public:
  long long numerator;//numerator 
  long long denominator;//denominator
  Fraction();
  Fraction(long long numer, long long denomin);

  void print();
  Fraction reduce();
  Fraction mul(Fraction a);
  Fraction add(Fraction a);
  int equal(Fraction a);
  int low_zero();
  Fraction minus();
  Fraction divide(Fraction a);
};

Fraction::Fraction() {
 numerator = 0;
 denominator = 1;
}

Fraction Fraction::reduce() {
  Fraction s;
  long long a = numerator;
  long long b = denominator;
  long long c;

  if(numerator == 0) return s;
  if(a < 0) a = -a;
  if(b < 0) b = -b;
  if(b > a) {
    long long t = a;
    a = b;
    b = t; 
  }
//  else {
//    a = denominator;
//    b = numerator;
//  }
  while(b !=0) {
    c = a % b;
    a = b;
    b = c;
  }  
  s.numerator = numerator/a;
  s.denominator = denominator/a;
  return s;
}

Fraction::Fraction(long long numer, long long denomin) { 
  numerator = numer;
  denominator = denomin;
  reduce();
}



int Fraction::equal(Fraction a) { //return 1 if equal a, 0 if not
  long long t1;
  long long t2;

  t1 = a.numerator * denominator;
  if(t1 / denominator != a.numerator){cout<<"failed"; exit(1);}
  t2 = a.denominator * numerator;
  if(t2 / a.denominator != numerator) {cout <<"failed"; exit(1);}
  return a.numerator * denominator == a.denominator * numerator;
}

int Fraction::low_zero() {
   long long t;
   t = numerator * denominator;
   if(t / denominator != numerator) {cout<<"failed"; exit(1);}
   if(numerator*denominator < 0) return 1;
   else return 0;
}

Fraction Fraction::mul(Fraction a) { //multiplication
  Fraction s;
  s.numerator = a.numerator * numerator;
  if(a.numerator != 0) {
    if(s.numerator / a.numerator != numerator) {cout<<"failed";exit(1);}
  }
  if(numerator != 0) {
    if(s.numerator / numerator != a.numerator) {cout<<"failed";exit(1);}
  }
  s.denominator = a.denominator * denominator;
  if (s.denominator / a.denominator != denominator) {cout<<"failed";exit(1);}
  s = s.reduce();
  return s;
}

Fraction Fraction::add(Fraction a) { //addition
  Fraction s; 

  long long left_numerator = a.numerator * denominator;
  if (left_numerator / denominator != a.numerator) {cout<<"failed";exit(1);}
  long long right_numerator = a.denominator * numerator;
  if (right_numerator / a.denominator != numerator) {cout<<"failed";exit(1);}
  s.numerator = left_numerator + right_numerator;
  if (s.numerator - left_numerator != right_numerator) {cout<<"failed";exit(1);}
  s.denominator = a.denominator * denominator;
  if (s.denominator / a.denominator != denominator) {cout<<"failed";exit(1);}
  s = s.reduce();
  return s;
}

void Fraction::print() { 
  cout << numerator << " "<< denominator;
}

Fraction Fraction::minus() { 
  Fraction s;
  s.numerator = -numerator;
  if(-s.numerator != numerator) {cout<<"failed";exit(1);}
  s.denominator = denominator;
  return s;
}

Fraction Fraction::divide(Fraction a) {
   Fraction s;
  s.numerator = numerator * a.denominator;
  if (s.numerator / a.denominator != numerator) {cout<<"failed";exit(1);}
  s.denominator = denominator * a.numerator;
  if (s.denominator / denominator != a.numerator) {cout<<"failed";exit(1);}
  s = s.reduce();
  return s;
}
//end class


typedef vector<Fraction> polynomial; 
typedef vector<Fraction> row;  
typedef vector<Fraction> column; 
typedef vector<row> matrix;
typedef vector<Fraction> polynomialh;// coefficients h_0,..,h_n  
typedef vector<Fraction> rowh;
typedef vector<rowh> matrixh;
typedef vector<Fraction> row_int;
typedef vector<row_int> matrix_int; //macierz liczbowa faktoryzacji (1)



int degree(polynomial a) {   
  int n = a.size()-1; 
  Fraction z = Fraction(0,1);

  while((a[n].equal(z)) && (n > 0)) n--; 
  return n;
}

void print_pol(polynomial a) { 
  int i = 0;
  while(i <= degree(a)) {
//    cout << a[i] << "x^" << i << " ";
    a[i].print();
    i++;
    cout << "  ";
  }
  cout << '\n';
}

polynomial add_pol(polynomial a, polynomial b) { //
  polynomial s(degree(a)+degree(b)+2);
  int i;
  
  i = 0;
  while((i <= degree(a)) || (i <= degree(b))) {
    //s[i] = a[i] + b[i];
    s[i]= a[i].add(b[i]);
    i++;
  }
  return s;
}

polynomial multiplication_pol(polynomial a, polynomial b) { 
  polynomial s(degree(a)+degree(b)+2);
  polynomial temp(degree(a)+degree(b)+2);
  matrix M(degree(a)+degree(b)+2, row(degree(a)+degree(b)+2));
  int i,j;

  for(i = 0; i <= degree(b); i++)
    for(j = 0; j <= degree(a); j++){
    // M[i][j+i] = b[i]*a[j];
      M[i][j+i] = a[j].mul(b[i]); 
}
 temp = M[0];

 for(i = 1; i <= degree(b); i++)
   temp = add_pol(temp, M[i]);
 return temp;
}

polynomial modulo_pol(polynomial a, polynomial b) { //return remainder of p/q
  int n = degree(a) - degree(b);
  int m = degree(a);
  polynomial sol(n+2);
  polynomial r(m+1);
  r = a;
  int i;
//dopisac przypadek jak deg(q) > deg(p)
  while(degree(r) >= degree(b) ) {
     int x = degree(r) - degree(b);
     //sol[x] = r[degree(r)]/b[degree(b)];
     sol[x] = r[degree(r)].divide(b[degree(b)]);
     for(i = 0; i <= degree(b); i++) {
         //r[i+x] = r[i+x] - (sol[x]*b[i]);   
        Fraction t1;
        t1 = sol[x].minus();
        Fraction t2;
        t2 = b[i].mul(t1); 
        r[i+x] = r[i+x].add(t2);
     }
  }
return r;
}

polynomial divide_pol(polynomial a, polynomial b) { //return result of division a(p)/b(p)
  int n = degree(a) - degree(b);
  int m = degree(a);
  polynomial sol(SIZE);
  polynomial r(SIZE);
  r = a;
  int i;
 
  if(degree(b) == 0)
    for(i = 0; i <= degree(a); i++) sol[i] = a[i].divide(b[0]); //sol[i] = a[i]/b[0];
else
  while(degree(r) >= degree(b)) {
     int x = degree(r) - degree(b); 
     //sol[x] = r[degree(r)]/b[degree(b)];
     sol[x] = r[degree(r)].divide(b[degree(b)]); 
    for(i = 0; i <= degree(b); i++) {
         //r[i+x] = r[i+x] - (sol[x]*b[i]);
      Fraction t1;
      t1 = sol[x].minus();
      Fraction t2;
      t2 = b[i].mul(t1);
      r[i+x] = r[i+x].add(t2);
     }
   }
return sol;
}

polynomial modulo_horner(polynomial a, polynomial b) { //return division remainder   
//Horner method
  polynomial s(SIZE);
  polynomial r(SIZE);

  s[degree(a)-1] = a[degree(a)];
  for(int i = degree(a) - 2; i >= 0; i--) {
//    s[i] =  a[i+1] - (b[0]*s[i + 1]);
    Fraction t1;
    t1 = s[i+1].minus();
    Fraction t2;
    t2 = b[0].mul(t1);
    s[i] = a[i+1].add(t2);
  }
  Fraction temp1 = s[0].minus();
  Fraction temp2 = b[0].mul(temp1);
  r[0] = a[0].add(temp2);
  return r;

}

polynomial divide_horner(polynomial a, polynomial b) { //return result of division a/b 
//Horner method
  polynomial s(SIZE);
  
  s[degree(a)-1] = a[degree(a)];
  for(int i = degree(a) - 2; i >= 0; i--) {
//    s[i] =  a[i+1] - (b[0]*s[i + 1]);
    Fraction t1;
    t1 = s[i+1].minus();
    Fraction t2;
    t2 = b[0].mul(t1);
    s[i] = a[i+1].add(t2);
  }
  return s;
}

polynomialh value_pol(polynomial a, Fraction q) { //return coefficients a_0,..,a_n for a(q)
  polynomialh b(SIZE);
  Fraction temp = Fraction(1,1);

  b[0] = a[0];
  for(int i = 1; i <= degree(a); i++) {
    //temp = temp * q;
    temp = temp.mul(q);
    //b[i] = temp * a[i];
    b[i] = temp.mul(a[i]);
  }
  return b;
} 

/*polynomial one_pol() { //zwraca wielomian wielomian jedynkowy
  polynomial a(SIZE);
  
  for(int i = 0; i < SIZE; i++)
    a[i] = 1;
  return a;
}*/

void printh(polynomialh h) { 
  for(int i = 0; i <= degree(h); i++) {
   // cout << h[i] << " h_" << i << " ";
 // cout << '\n';
    h[i].print(); 
    cout << " h_" << i << " ";
  }
}

void printx(polynomialh x) { 
  for(int i = 0; i <= degree(x); i++) {
  //  cout << x[i] << " x_" << i << " ";
 // cout << '\n';
    x[i].print(); 
    cout << " x_" << i << " ";
  }
}


polynomialh create_pol(int d) {//create polynomial h_0,...,h_n of the degree equal to d (coefficients equal to one)
  polynomialh a(SIZE);
  for(int i = 0; i <=d; i++)
    a[i] = Fraction(1,1);
  return a;
}

Fraction root_pol(polynomial a){ // return root oint of the linear polynomial 
//a(p) = p + a_i
  Fraction b;
  
  b = a[0].minus();
return b;
}

/*matrix_int create_matrix_id(int n){ //tworzy macierz id rozmiaru nxn
  matrix_int A(n,row_int(n));

  for(int i = 0; i < n; i++) A[i][i] = 1;
  return A;
}*/

void print_matrix_int(matrix_int A, int n, int m){
  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++){
   //   cout << " " << A[i][j];
       A[i][j].print();     
       cout << " ";
    }
    cout << '\n';
    }
}
/*
void initialize() {
  //srand((unsigned)time(0));
  std::random_device rd;
  std::mt19937 gen(rd());
}

long long get_number(long long range1, long long range2) { //losowanie z przedzialu range1 - range2
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> dis(range1,range2);
  return dis(gen);
}

Fraction get_fraction() { //ustalanie przedzialu losowania
  long long denominator = get_number(1, 10);
  long long numerator = get_number(1,4 * denominator) - 2 * denominator;
  return Fraction(numerator, denominator); 
}

int fraction_in_vector(Fraction f, vector<Fraction> v) { //zwraca 1 gdy punkt sie powtarza
  for(int i = 0; i < v.size(); i++)
    if(f.equal(v[i]))
      return 1;
  return 0;
}

Fraction get_fraction_no_repeat(vector<Fraction> except) { //losowanie roznych punktow
  Fraction f = get_fraction();
  while(fraction_in_vector(f, except))
    f = get_fraction();
  return f;
}
*/
int main(int argc, char* argv[]) {


 if(argc < 3) {
   cout << "Not enough arguments \n";
   return 1;
}

 int Dh = atoi(argv[1]) - 1; //size of kernel (polynomial h)
 int Dx = atoi(argv[2]) - 1; // size of output (polynomial x)
 K = Dh+Dx+1; //degree of the polynomial m(p) 

 if(argc < 2 * (K-1) + 3) {
   cout << "Not enough arguments";
   return 1;
}



 polynomialh h(SIZE); //polynomial h (kernel) 
 polynomialh x(SIZE); //polynomial x (output)
 matrix m_i(K, row(SIZE)); //matrix of the linear polynomials m_0(p),..., m_k(p), m_i(p) = p + a_i
 polynomial m(SIZE); // product of m_0(p),..., m_k(p)
 matrix M_i(K, row(SIZE)); // matrix of the polynomials M_i(p),... M_i(p) (M_i(p)=m(p)/m_i(p))
 matrix r_i(K, row(SIZE)); // matrix of thepolynomials M_i(p) mod m_i(p)
 matrixh Vh_i(K, rowh(SIZE)); //matrix of the polynomials with coefficients h_0,...,h_n computed  
//as h(p) mod m_i(p) from Bezou theorem = h(a_i) where a_i is a root point of m_i(p)
 matrixh Vx_i(K, rowh(SIZE)); // matrix of the polynomials with coefficients x_0,..., x_n computed 
//as x(p) mod m_i(p) from Bezou theorem = x(a_i) where a_i is a root point of m_i(p) 
//matrix_int S(SIZE, row_int(SIZE)); 
 matrixh B(K, rowh(SIZE));

h = create_pol(Dh);  //polynomial of the degree Dh
x = create_pol(Dx);  //polynomial of the degree Dx



 for(int i = 0; i < K-1; i++) {//load polynomials m_i(p) - coefficient as Fraction
   long long m = atoi(argv[3 + 2 * i]);
   long long n = atoi(argv[3 + 2 * i + 1]);
   //Fraction  f = Fraction(m,n);
   m_i[i][0] = Fraction(-m,n);
   m_i[i][1] = Fraction(1,1);
}
//koniec wczytywania z klawiatury


//product of the polynomials m_i
m = m_i[0]; 
  for(int i = 1; i < K-1; i++)
    m = multiplication_pol(m, m_i[i]);
  for(int i = 0; i < K-1; i++){
    M_i[i] = divide_horner(m, m_i[i]);  // M_i(p)
//print_pol(M_i[i]);
    r_i[i] = modulo_horner(M_i[i], m_i[i]); // r_i for - M_i[i] mod m_i[i]
    Vh_i[i] = value_pol(h, root_pol(m_i[i])); //h_i(p) = h(p)modm_i(p) for m_i(p)=p+a
    Vx_i[i] = value_pol(x, root_pol(m_i[i])); //x_i(p) = x(p)modm_i(p) for m_i(p)=p+a
  }


 for(int i = 0; i < K-1; i++) { //
   for(int j = 0; j < K-1; j++)  
     B[i][j] = M_i[i][j]; }
 
 for(int i = 0; i < K; i++)  
   B[i][K-1] = Fraction(0,1);
 for(int j = 0; j < K; j++)
   B[K-1][j] = m[j]; 

ofstream myfile ("BTMat.txt");
if (myfile.is_open()){
  for(int i = 0; i < K; i++){
    for(int j = 0; j < K; j++)
        myfile << B[i][j].numerator << " " << B[i][j].denominator << "  ";
    myfile << '\n';
}
myfile.close();
}

for(int i = 0; i < K-1; i++) {
  for(int j = 0; j < Dh+1; j++){
   //cout << 1/r_i[i][0] * Vh_i[i][j] << " ";
   Vh_i[i][j] = Vh_i[i][j].divide(r_i[i][0]); //dzielenie przez r
//   Vh_i[i][j].print();
//    cout << " ";
   }
//  cout << '\n';
}


for(int j = 0; j < K; j++) 
  Vh_i[K-1][j] = Fraction(0,1);
Vh_i[K-1][Dh] = Fraction(1,1);

ofstream myfile1 ("GMat.txt");
if(myfile1.is_open()){
  for(int i = 0; i < K; i++){
    for(int j = 0; j < Dh+1; j++)
      myfile1 << Vh_i[i][j].numerator << " " << Vh_i[i][j].denominator << "  ";
    myfile1 << '\n';
  }
  myfile1.close();
}


for(int j = 0; j < Dx+1; j++) 
  Vx_i[K-1][j] = Fraction(0,1);
Vx_i[K-1][Dx] = Fraction(1,1);

ofstream myfile2 ("ATMat.txt");
if(myfile2.is_open()){
  for(int j = 0; j < Dx+1; j++){
    for(int i = 0; i < K; i++)
      myfile2 << Vx_i[i][j].numerator << " " << Vx_i[i][j].denominator << "  ";
    myfile2 << '\n';
  }
  myfile2.close(); 
}

return 0;
}
