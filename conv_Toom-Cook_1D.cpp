#include <iostream>
#include <vector>
#include <fstream>
#include <math.h>
#include <cmath>
#include <iomanip>
#include <random>

using namespace std;

#define SIZE 40

typedef vector<float> row;  
typedef vector<row> matrix;
typedef vector<double> rowd;
typedef vector<rowd> matrixd;

void print_matrix(matrix A, int n, int m) { 
  for(int i = 0; i < n; i++) {
    for(int j = 0; j < m; j++)
      cout << std::fixed << std::setprecision(12) << A[i][j] << "  ";
    cout << '\n';
  }
}

void print_vector(row a, int n) { 
  for(int i = 0; i < n; i++)
    cout << std::setprecision(12) << a[i] << " ";
}

/*
matrix multiplication_matrix(matrix A, matrix B, int n, int m, int l) { //mnozy macierz A n x l i 
//macierz B l x m
  matrix C(SIZE*SIZE, row(SIZE*SIZE));
  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++)
      for(int k = 0; k < l; k++){
        C[i][j] = C[i][j] + (A[i][k]*B[k][j]);
      }
  return C;
}
*/

//element drzewa
class TreeNode {
  public:
        double key; //order of magnitude - used in priority queue
        float add_res; //summation result in subtree
	TreeNode *left;
	TreeNode *right;
	
	TreeNode(double k, float f); 
	double element(); //return key
        float result(); //return result of summation
};

TreeNode::TreeNode(double k, float f) {
  key = k; 
  add_res = f;
  left = NULL;
  right = NULL;
}
 
double TreeNode::element() {
  return key;
}

float TreeNode::result() {
  return add_res;
}

//koniec klasy

void inorder(TreeNode* p) { //print elements of the tree in inorder order 
  if(p == NULL) { 
cout << "NULL";
return;
}
  inorder(p->left);
  cout << p->element() << "/" << p->result() << "  ";
  inorder(p->right);
}


//Tree 
class Tree {
public:
  TreeNode* root; //pointer to a root
  
  Tree(double k, float f);
  void print_inorder();
};	

Tree::Tree(double k, float f) {
  root = new TreeNode(k,f);
}

void Tree::print_inorder() {
  inorder(root);
}

//element of priority queue
class QueueNode {  //element kolejki
  public:
   Tree* root; //pointer to the tree 
   QueueNode *next;
   
   QueueNode(Tree* T);
};

QueueNode::QueueNode(Tree* T) {  
  root = T;
  next = NULL;  
}

//priority queue
class Queue { 
  public:
  QueueNode* Head;
  
  Queue();
  QueueNode* first();
  QueueNode* last();
  double element(QueueNode* p);
  void print();
  void insert (Tree* T);
  void qdelete();
//  int is_empty();
  
  };
  
Queue::Queue() {
	Head = new QueueNode(NULL);
  }
  
QueueNode* Queue::first() { //return first element position (pointer to Head)
  return Head;
}
	
QueueNode* Queue::last() { //return after last element position (pointer to the last elemenet)
  QueueNode *temp = Head;
  while(temp->next != NULL) temp = temp->next;
  return temp;
}

double Queue::element(QueueNode* el) { //return key
 // if(el->next == NULL) return NIL;
  Tree* temp = el->next->root;
  return temp->root->key;
}
  
void Queue::print() {
   QueueNode *temp;
   temp = Head;
   while(temp != last()) {
 	   cout << element(temp) << " ";
       temp = temp->next;
   }
   cout << '\n';
 }
  
 void Queue::insert(Tree* T) { //insert tree to the queue
  QueueNode* temp;
  temp = new QueueNode(T);

  QueueNode *temp1;
  temp1 = Head;
  while((temp1 != last()) && (element(temp1) < temp->root->root->key)) temp1 = temp1-> next;
  QueueNode *p;
  p = temp1 -> next;
  temp1->next = temp;
  temp1->next->next = p;
 }

void Queue::qdelete() { //delete tree from the queue
  QueueNode* temp;
  
  temp = Head->next;
  if(temp != NULL) {
    Head->next = temp->next;
    delete temp;
  }
}
//end of queue class

Tree* merge(Tree* p, Tree* q) { //merge two trees
  Tree* temp;
  float v;
  v = p->root->result() + q->root->result();
  //temp = new Tree(abs(log2(abs(v))), v);
  //temp = new Tree(abs(log2(p->root->element()+q->root->element())),v);
  temp = new Tree(p->root->element()+q->root->element(), v);
  temp->root->left = p->root;
  temp->root->right = q->root;
  return temp;
}


row multiplication_huffman(matrix A, int n, int m, row b) { //multiplication of matrix A (n x m) and vector b (m)
  row c(SIZE);

  for(int i = 0; i < n; i++){ //for every row
    Queue Q;
    for(int j = 0; j < m; j++) {
      float f;
      double e;
      e = log2(abs(A[i][j]));
      Tree* t = new Tree(e, A[i][j]*b[j]);
      Q.insert(t);
	}
      QueueNode* p = Q.first();
      while(p->next != Q.last()) {
        Tree* t = new Tree(0,0); 
        t = merge(p->next->root, p->next->next->root);
        Q.qdelete();
        Q.qdelete();
        Q.insert(t);
      }
      c[i] = p->next->root->root->result();
  }
  return c;
}

//huffman for double
//tree element
class TreeNode_d {
  public:
        double key_d; //order of magnitude - 
        double add_res_d; //result of summation in subtree
	TreeNode_d *left_d;
	TreeNode_d *right_d;
	
	TreeNode_d(double k, double f); 
	double element_d(); //return key
        double result_d(); //return result of summation
};

TreeNode_d::TreeNode_d(double k, double f) {
  key_d = k; 
  add_res_d = f;
  left_d = NULL;
  right_d = NULL;
}
 
double TreeNode_d::element_d() {
  return key_d;
}

double TreeNode_d::result_d() {
  return add_res_d;
}

//end

//void inorder(TreeNode* p) { //wypisywanie drzewa w porzadku inorder
//  if(p == NULL) { 
//cout << "NULL";
//return;
//}
//  inorder(p->left);
//  cout << p->element() << "/" << p->result() << "  ";
//  inorder(p->right);
//}


//Tree 
class Tree_d {
public:
  TreeNode_d* root_d; //pointer to the root
  
  Tree_d(double k, double f);
  //  void print_inorder();
};	

Tree_d::Tree_d(double k, double f) {
  root_d = new TreeNode_d(k,f);
}

//void Tree::print_inorder() {
//  inorder(root);
//}

//element of priority queue
class QueueNode_d {  //element kolejki
  public:
   Tree_d* root_d; //pointer to a tree 
   QueueNode_d *next_d;
   
   QueueNode_d(Tree_d* T);
};

QueueNode_d::QueueNode_d(Tree_d* T) {  
  root_d = T;
  next_d = NULL;  
}

//priority queue
class Queue_d { 
  public:
  QueueNode_d* Head_d;
  
  Queue_d();
  QueueNode_d* first_d();
  QueueNode_d* last_d();
  double element_d(QueueNode_d* p);
  void print_d();
  void insert_d (Tree_d* T);
  void qdelete_d();
//  int is_empty();
  
  };
  
Queue_d::Queue_d() {
	Head_d = new QueueNode_d(NULL);
  }
  
QueueNode_d* Queue_d::first_d() { 
  return Head_d;
}
	
QueueNode_d* Queue_d::last_d() { 
  QueueNode_d *temp = Head_d;
  while(temp->next_d != NULL) temp = temp->next_d;
  return temp;
}

double Queue_d::element_d(QueueNode_d* el) { //return key
  //emy kolejke, z pozycji el
 // if(el->next_d == NULL) return NIL;
  Tree_d* temp = el->next_d->root_d;
  return temp->root_d->key_d;
}
  
void Queue_d::print_d() {
   QueueNode_d *temp;
   temp = Head_d;
   while(temp != last_d()) {
 	   cout << element_d(temp) << " ";
       temp = temp->next_d;
   }
   cout << '\n';
 }
  
 void Queue_d::insert_d(Tree_d* T) { //insert tree to the priority queue
  QueueNode_d* temp;
  temp = new QueueNode_d(T);

  QueueNode_d *temp1;
  temp1 = Head_d;
  while((temp1 != last_d()) && (element_d(temp1) < temp->root_d->root_d->key_d)) temp1 = temp1-> next_d;
  QueueNode_d *p;
  p = temp1 -> next_d;
  temp1->next_d = temp;
  temp1->next_d->next_d = p;
 }

void Queue_d::qdelete_d() { //delete tree from the priority queue
  QueueNode_d* temp;
  
  temp = Head_d->next_d;
  if(temp != NULL) {
    Head_d->next_d = temp->next_d;
    delete temp;
  }
}
//end of queue class

Tree_d* merge_d(Tree_d* p, Tree_d* q) { //merge two trees
  Tree_d* temp;
  double v;
  v = p->root_d->result_d() + q->root_d->result_d();
  //temp = new Tree(abs(log2(abs(v))), v);
  //temp = new Tree(abs(log2(p->root->element()+q->root->element())),v);
  temp = new Tree_d(p->root_d->element_d()+q->root_d->element_d(), v);
  temp->root_d->left_d = p->root_d;
  temp->root_d->right_d = q->root_d;
  return temp;
}


rowd multiplication_huffman_d(matrixd A, int n, int m, rowd b) { 
  rowd c(SIZE);

  for(int i = 0; i < n; i++){ 
    Queue_d Q;
    for(int j = 0; j < m; j++) {
      double f;
      double e;
      e = log2(abs(A[i][j]));
      Tree_d* t = new Tree_d(e, A[i][j]*b[j]);
      Q.insert_d(t);
	}
      QueueNode_d* p = Q.first_d();
      while(p->next_d != Q.last_d()) {
        Tree_d* t = new Tree_d(0,0); 
        t = merge_d(p->next_d->root_d, p->next_d->next_d->root_d);
        Q.qdelete_d();
        Q.qdelete_d();
        Q.insert_d(t);
      }
      c[i] = p->next_d->root_d->root_d->result_d();
  }
  return c;
}




row multiplication_wise(row a, row b, int n) { 
  row c(SIZE);

  for(int i = 0; i < n; i++)
      c[i] = a[i]*b[i];
  return c;
}

rowd multiplication_wise_d(rowd a, rowd b, int n) { 
  row c(SIZE);
  rowd result(SIZE);

  for(int i = 0; i < n; i++) {
      c[i] = (float)a[i]*(float)b[i];
      result[i] = (double)c[i];
  }
  return result;
}

row multiplication(matrix A, int n, int m, row b) {
  row c(SIZE);

  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++) {
      c[i] = c[i] + A[i][j]*b[j];
  }
  return c;
}

rowd multiplication_d(matrixd A, int n, int m, rowd b) {
  rowd c(SIZE);

  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++) {
      c[i] = c[i] + A[i][j]*b[j];
  }
  return c;
}
/*
matrix transp(matrix A, int n, int m) { //zwraca macierz transponowana dla macierzy A nxm
  matrix AT(SIZE, row(SIZE));

  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      AT[i][j] = A[j][i];
  return AT;
}*/

int main(int argc, char* argv[]) {

if(argc != 3){
  cout << "Bad number of arguments \n";
  return 1;
}

int L = 5000; //number of loops
int n_h = atoi(argv[1]); //size of the kernel
int n_o = atoi(argv[2]); //size of the output

int n_i = n_h + n_o -1; //size of the input

matrix G(SIZE, row(SIZE)); //matrix G
matrix BT(SIZE, row(SIZE)); //matrix B^T
matrix AT(SIZE, row(SIZE)); //matrix A^T 

matrixd Gd(SIZE, rowd(SIZE)); //matrix G
matrixd BTd(SIZE, rowd(SIZE)); //matrix B^T
matrixd ATd(SIZE, rowd(SIZE)); //matrix A^T 


row g(SIZE); 
row b(SIZE);
row a(SIZE);
row w(SIZE);
row gh(SIZE); 
row bh(SIZE);
row ah(SIZE);
row wh(SIZE);
rowd gd(SIZE); 
rowd bd(SIZE);
rowd ad(SIZE);
rowd wd(SIZE);

row h(SIZE); //kernel
row x(SIZE); //input
rowd hd(SIZE); //kernel in double
rowd xd(SIZE); //input in double
row s(SIZE); //direct convolution
rowd d(SIZE); //direct convolution in double 

ifstream fBT("./BTMat.txt");
  for(int i = 0; i < n_i; i++) 
    for(int j = 0; j < n_i; j++) {
      long long num;
      long long den;
      fBT >> num >> den;
      BT[i][j] =(float)num/(float)den;
        }
  fBT.close();
//print_matrix(BT,W+(2*NL), W+NL);

for(int i = 0; i < n_i; i++)
  for(int j = 0; j < n_i; j++)
    BTd[i][j] = (double)BT[i][j];

ifstream fAT("./ATMat.txt");
  for(int i = 0; i < n_o; i++) 
    for(int j = 0; j < n_i; j++) {
      long long num;
      long long den;
      fAT >> num >> den;
      AT[i][j] = (float)num/(float)den;
        }
  fAT.close();

for(int i = 0; i < n_o; i++)
  for(int j = 0; j < n_i; j++)
    ATd[i][j] = (double)AT[i][j];

ifstream fG("GMat.txt");
  for(int i = 0; i < n_i; i++) 
    for(int j = 0; j < n_h; j++) {
      long long num;
      long long den;
      fG >> num >> den;
      G[i][j] = (float)num/(float)den;
        }
  fG.close();

for(int i = 0; i < n_i; i++)
  for(int j = 0; j < n_h; j++)
    Gd[i][j] = (double)G[i][j];

double error_Wh_all = 0; //winograd huffman in float v direct in double
double error_W_all = 0; //winograd in float v direct in double
double error_d_all = 0; // direct in float v direct in double
double error_Wd_all = 0; //winograd in mixed v direct in double

for(int it = 0; it < L; it++) {
  for(int i = 0; i < n_o; i++){
      s[i] = 0;
      d[i] = 0;
}

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<float> dis(-1,1);

  for(int i = 0; i < n_h; i++){
    h[i] =  dis(gen);
    hd[i] = (double)h[i];
//    cout << h[i] << " ";
  }
//cout << "\n";


  for(int i = 0; i < n_i; i++) {
    x[i] = dis(gen);
    xd[i] = (double)x[i];
//    cout << x[i];
  }
//cout << "\n";

/*
  h[0] = hd[0] = 1;
  h[1] = hd[1] = 2;
  h[2] = hd[2] = 1/2;

  x[0] = xd[0] = 1;
  x[1] = xd[1] = 1;
  x[2] = xd[2] = 1;
  x[3] = xd[3] = 1;
  x[4] = xd[4] = 1;
  x[5] = xd[5] = 1;
  x[6] = xd[6] = 1;
  x[7] = xd[7] = 1;
  x[8] = xd[8] = 1;
  x[9] = xd[9] = 1;
  x[10] = xd[10] = 1;
  x[11] =xd[11] = 1;
*/  
  gh = multiplication_huffman(G, n_i, n_h, h);
   g = multiplication(G, n_i, n_h, h);
  gd = multiplication_huffman_d(Gd, n_i, n_h, hd);  

//  print_vector(g, n_i);
//  cout << endl;

  bh = multiplication_huffman(BT, n_i, n_i, x);
   b = multiplication(BT, n_i, n_i, x);
  bd = multiplication_huffman_d(BTd, n_i, n_i, xd);

//  print_vector(b, n_i);
//  cout << endl;
  
  wh = multiplication_wise(gh, bh, n_i);
   w = multiplication_wise(g, b, n_i);
  wd = multiplication_wise_d(gd, bd, n_i);
  
//  print_vector(w, n_i);
//  cout << endl;
  
  ah = multiplication_huffman(AT, n_o, n_i, wh);
   a = multiplication(AT, n_o, n_i, w);
  ad = multiplication_huffman_d(ATd, n_o, n_i, wd);

//  print_vector(a, n_o);
//  cout << endl;
  
  for(int i = 0; i < n_o; i++)
      for(int k = 0; k < n_h; k++)
           s[i] = s[i] + (h[k]*x[k+i]);

	   
//	   print_vector(s, n_o);
//	   cout << endl;
  for(int i = 0; i < n_o; i++)
    for(int k = 0; k < n_h; k++)
         d[i] = d[i] + (hd[k]*xd[k+i]);


//for(int i = 0; i < n_o; i++) cout << a[i] << " ";
//cout << endl;
//for(int i = 0; i < n_o; i++) cout << d[i] << " ";
//cout << endl;


  double error_Wh_g = 0; //Winograd with Huffman 
  double error_d_g = 0; //direct
  double error_W_g = 0; //Winograd
  double error_Wd = 0; //Mixed with Huffman
//double error_W_d = 0;

  for(int i = 0; i < n_o; i++){
    error_Wh_g = error_Wh_g + abs(ah[i] - d[i]);
    error_d_g = error_d_g + abs(s[i]-d[i]);
    error_W_g = error_W_g + abs(a[i] - d[i]);
    error_Wd = error_Wd + abs(ad[i] - d[i]);
//    error_W_d = error_W_d + pow((a[i] - s[i]), 2);
  }
  //error_Wh_g = sqrt(error_Wh_g);
  //  error_d_g = sqrt(error_d_g);
  //error_W_g = sqrt(error_W_g);
  //error_Wd = sqrt(error_Wd);
//error_W_d = sqrt(error_W_d);

  error_Wh_all = error_Wh_all + error_Wh_g;
  error_d_all = error_d_all + error_d_g;
  error_W_all = error_W_all + error_W_g;
  error_Wd_all = error_Wd_all + error_Wd;
}
error_Wh_all = error_Wh_all/L;
error_d_all = error_d_all/L;
error_W_all = error_W_all/L;
error_Wd_all = error_Wd_all/L;

//double error_W_output = error_W_all/(double)n_o;
 cout << " Winograd_huffman: "  << std::setprecision(12) << std::fixed << error_Wh_all << 
 " Mixed+Huffman: " << error_Wd_all << "Winograd: " << error_W_all << " Direct: " << error_d_all << endl;
  //" Winograd: "  << error_W_all << endl << " Double+float " << error_Wd_all << " Direct: " << error_d_all << '\n'; 
//" Winograd v direct: " << error_W_d << '\n';

return 0;
}
