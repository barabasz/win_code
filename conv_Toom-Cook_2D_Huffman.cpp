#include <iostream>
#include <vector>
#include <fstream>
#include <math.h>
#include <cmath>
#include <iomanip>
#include <random>

using namespace std;

#define SIZE 40

typedef vector<float> row;  
typedef vector<row> matrix;
typedef vector<double> rowd;
typedef vector<rowd> matrixd;

void print_matrix(matrix A, int n, int m) { 
  for(int i = 0; i < n; i++) {
    for(int j = 0; j < m; j++)
      cout << std::fixed << std::setprecision(12) << A[i][j] << "  ";
    cout << '\n';
  }
}

void print_vector(row a, int n) { 
  for(int i = 0; i < n; i++)
    cout << a[i] << " ";
}

//element drzewa
class TreeNode {
  public:
        double key; //order of magnitude 
        float add_res; //result of summation in subtree
	TreeNode *left;
	TreeNode *right;
	
	TreeNode(double k, float f); 
	double element(); //return key
        float result(); //return summation result
};

TreeNode::TreeNode(double k, float f) {
  key = k; 
  add_res = f;
  left = NULL;
  right = NULL;
}
 
double TreeNode::element() {
  return key;
}

float TreeNode::result() {
  return add_res;
}



void inorder(TreeNode* p) { //print tree in inorder order
  if(p == NULL) { 
cout << "NULL";
return;
}
  inorder(p->left);
  cout << p->element() << "/" << p->result() << "  ";
  inorder(p->right);
}


//tree
class Tree {
public:
  TreeNode* root; //pointer to the root
  
  Tree(double k, float f);
  void print_inorder();
};	

Tree::Tree(double k, float f) {
  root = new TreeNode(k,f);
}

void Tree::print_inorder() {
  inorder(root);
}

//element of priority queue
class QueueNode {  //element of the queue
  public:
   Tree* root; //pointer to the tree 
   QueueNode *next;
   
   QueueNode(Tree* T);
};

QueueNode::QueueNode(Tree* T) {  
  root = T;
  next = NULL;  
}

//priority queue
class Queue { 
  public:
  QueueNode* Head;
  
  Queue();
  QueueNode* first();
  QueueNode* last();
  double element(QueueNode* p);
  void print();
  void insert (Tree* T);
  void qdelete();
//  int is_empty();
  
  };
  
Queue::Queue() {
	Head = new QueueNode(NULL);
  }
  
QueueNode* Queue::first() { 
  return Head;
}
	
QueueNode* Queue::last() { 
  QueueNode *temp = Head;
  while(temp->next != NULL) temp = temp->next;
  return temp;
}

double Queue::element(QueueNode* el) { //return key
 // if(el->next == NULL) return NIL;
  Tree* temp = el->next->root;
  return temp->root->key;
}
  
void Queue::print() {
   QueueNode *temp;
   temp = Head;
   while(temp != last()) {
 	   cout << element(temp) << " ";
       temp = temp->next;
   }
   cout << '\n';
 }
  
 void Queue::insert(Tree* T) { //insert tree to the queue
  QueueNode* temp;
  temp = new QueueNode(T);

  QueueNode *temp1;
  temp1 = Head;
  while((temp1 != last()) && (element(temp1) < temp->root->root->key)) temp1 = temp1-> next;
  QueueNode *p;
  p = temp1 -> next;
  temp1->next = temp;
  temp1->next->next = p;
 }

void Queue::qdelete() { //delete tree from the queue
  QueueNode* temp;
  
  temp = Head->next;
  if(temp != NULL) {
    Head->next = temp->next;
    delete temp;
  }
}
//koniec klasy kolejka

Tree* merge(Tree* p, Tree* q) { //merge two trees
  Tree* temp;
  float v;
  v = p->root->result() + q->root->result();
  //temp = new Tree(abs(log2(abs(v))), v);
  //temp = new Tree(abs(log2(p->root->element()+q->root->element())),v);
  temp = new Tree(p->root->element()+q->root->element(), v);
  temp->root->left = p->root;
  temp->root->right = q->root;
  return temp;
}


float multiplication_left_huffman(row a, int n, row b) { //huffman summation for left row
  float c;

//  for(int i = 0; i < n; i++){ 
    Queue Q;
    for(int j = 0; j < n; j++) {
      float f;
      double e;
      e = log2(abs(a[j]));
      Tree* t = new Tree(e, a[j]*b[j]);
      Q.insert(t);
	}
      QueueNode* p = Q.first();
      while(p->next != Q.last()) {
        Tree* t = new Tree(0,0); 
        t = merge(p->next->root, p->next->next->root);
        Q.qdelete();
        Q.qdelete();
        Q.insert(t);
      }
      c = p->next->root->root->result();
  return c;
}

float multiplication_right_huffman(row a, int n, row b) { //huffman summation for right row
  float c;

//  for(int i = 0; i < n; i++){ //dla kazdego wiersza macierzy
    Queue Q;
    for(int j = 0; j < n; j++) {
      float f;
      double e;
      e = log2(abs(b[j]));
      Tree* t = new Tree(e, a[j]*b[j]);
      Q.insert(t);
	}
      QueueNode* p = Q.first();
      while(p->next != Q.last()) {
        Tree* t = new Tree(0,0); 
        t = merge(p->next->root, p->next->next->root);
        Q.qdelete();
        Q.qdelete();
        Q.insert(t);
      }
      c = p->next->root->root->result();
  return c;
}

matrix multiplication_matrix_left_huffman(matrix A, matrix B, int n, int m, int l) {
  matrix result(n, row(m));

  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++) {
       row a1(l);
	   row a2(l);
       for(int it = 0; it < l; it++) {
		   a1[it] = A[i][it];
		   a2[it] = B[it][j];
	   }
	   float b;
//cout << "d" << endl;
       b = multiplication_left_huffman(a1, l, a2);
//cout << "d" << endl;
       result[i][j] = b;
  }
//cout << "d" << endl;
  return result;  
}

matrix multiplication_matrix_right_huffman(matrix A, matrix B, int n, int m, int l) {
  matrix result(n, row(m));

  for(int i = 0; i < n; i++) 
    for(int j = 0; j < m; j++){//kolumny macierzy B
      row a1(l);
	  row a2(l);
      for(int it = 0; it < l; it++) {
		  a1[it] = A[i][it];
		  a2[it] = B[it][j];
	  }
    float b;
//cout << "d" << endl;
    b = multiplication_right_huffman(a1, l, a2);
//cout << "d" << endl;
    result[i][j] = b;
  }
//cout << "d" << endl;
  return result;  
}


//huffman double
//tree element
class TreeNode_d {
  public:
        double key_d; //order of magnitude 
        double add_res_d; //summation result in subtree
	TreeNode_d *left_d;
	TreeNode_d *right_d;
	
	TreeNode_d(double k, double f); 
	double element_d(); //return key
        double result_d(); //return summation result
};

TreeNode_d::TreeNode_d(double k, double f) {
  key_d = k; 
  add_res_d = f;
  left_d = NULL;
  right_d = NULL;
}
 
double TreeNode_d::element_d() {
  return key_d;
}

double TreeNode_d::result_d() {
  return add_res_d;
}

//class end

//void inorder(TreeNode* p) { //wypisywanie drzewa w porzadku inorder
 // if(p == NULL) { 
//cout << "NULL";
//return;
//}
//  inorder(p->left);
//  cout << p->element() << "/" << p->result() << "  ";
//  inorder(p->right);
//}


//tree 
class Tree_d {
public:
  TreeNode_d* root_d; //pointer to the root
  
  Tree_d(double k, double f);
//  void print_inorder();
};	

Tree_d::Tree_d(double k, double f) {
  root_d = new TreeNode_d(k,f);
}

//void Tree::print_inorder() {
 // inorder(root);
//}

//element of the priority queue
class QueueNode_d {  //element of the queue
  public:
   Tree_d* root_d; //pointer to the root
   QueueNode_d *next_d;
   
   QueueNode_d(Tree_d* T);
};

QueueNode_d::QueueNode_d(Tree_d* T) {  
  root_d = T;
  next_d = NULL;  
}

//priority queue
class Queue_d { 
  public:
  QueueNode_d* Head_d;
  
  Queue_d();
  QueueNode_d* first_d();
  QueueNode_d* last_d();
  double element_d(QueueNode_d* p);
  void print_d();
  void insert_d (Tree_d* T);
  void qdelete_d();
//  int is_empty();
  
  };
  
Queue_d::Queue_d() {
	Head_d = new QueueNode_d(NULL);
  }
  
QueueNode_d* Queue_d::first_d() { 
  return Head_d;
}
	
QueueNode_d* Queue_d::last_d() { 
  QueueNode_d *temp = Head_d;
  while(temp->next_d != NULL) temp = temp->next_d;
  return temp;
}

double Queue_d::element_d(QueueNode_d* el) { //return key 
 // if(el->next == NULL) return NIL;
  Tree_d* temp = el->next_d->root_d;
  return temp->root_d->key_d;
}
  
void Queue_d::print_d() {
   QueueNode_d *temp;
   temp = Head_d;
   while(temp != last_d()) {
 	   cout << element_d(temp) << " ";
       temp = temp->next_d;
   }
   cout << '\n';
 }
  
 void Queue_d::insert_d(Tree_d* T) { //insert tree to the queue
  QueueNode_d* temp;
  temp = new QueueNode_d(T);

  QueueNode_d *temp1;
  temp1 = Head_d;
  while((temp1 != last_d()) && (element_d(temp1) < temp->root_d->root_d->key_d)) temp1 = temp1-> next_d;
  QueueNode_d *p;
  p = temp1 -> next_d;
  temp1->next_d = temp;
  temp1->next_d->next_d = p;
 }

void Queue_d::qdelete_d() { //delete tree from the queue
  QueueNode_d* temp;
  
  temp = Head_d->next_d;
  if(temp != NULL) {
    Head_d->next_d = temp->next_d;
    delete temp;
  }
}
//koniec klasy kolejka

Tree_d* merge_d(Tree_d* p, Tree_d* q) { //merge two trees
  Tree_d* temp;
  double v;
  v = p->root_d->result_d() + q->root_d->result_d();
  //temp = new Tree(abs(log2(abs(v))), v);
  //temp = new Tree(abs(log2(p->root->element()+q->root->element())),v);
  temp = new Tree_d(p->root_d->element_d()+q->root_d->element_d(), v);
  temp->root_d->left_d = p->root_d;
  temp->root_d->right_d = q->root_d;
  return temp;
}

double multiplication_left_huffman_d(rowd a, int n, rowd b) { 
  double c;

 // for(int i = 0; i < n; i++){
    Queue_d Q;
    for(int j = 0; j < n; j++) {
      double f;
      double e;
      e = log2(abs(a[j]));
      Tree_d* t = new Tree_d(e, a[j]*b[j]);
      Q.insert_d(t);
	}
      QueueNode_d* p = Q.first_d();
      while(p->next_d != Q.last_d()) {
        Tree_d* t = new Tree_d(0,0); 
        t = merge_d(p->next_d->root_d, p->next_d->next_d->root_d);
        Q.qdelete_d();
        Q.qdelete_d();
        Q.insert_d(t);
      }
      c = p->next_d->root_d->root_d->result_d();
  return c;
}

double multiplication_right_huffman_d(rowd a, int n, rowd b) { 
  double c;

 // for(int i = 0; i < n; i++){ 
    Queue_d Q;
    for(int j = 0; j < n; j++) {
      double f;
      double e;
      e = log2(abs(b[j]));
      Tree_d* t = new Tree_d(e, a[j]*b[j]);
      Q.insert_d(t);
	}
      QueueNode_d* p = Q.first_d();
      while(p->next_d != Q.last_d()) {
        Tree_d* t = new Tree_d(0,0); 
        t = merge_d(p->next_d->root_d, p->next_d->next_d->root_d);
        Q.qdelete_d();
        Q.qdelete_d();
        Q.insert_d(t);
      }
      c = p->next_d->root_d->root_d->result_d();
  return c;
}


matrixd multiplication_matrix_left_huffman_d(matrixd A, matrixd B, int n, int m, int l) {
  matrixd result(n, rowd(m));

  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++) {
      rowd a1(l);
	  rowd a2(l);
      for(int it = 0; it < l; it++) {
		  a1[it] = A[i][it];
		  a2[it] = B[it][j];
	  }
      double b;
//cout << "d" << endl;
    b = multiplication_left_huffman_d(a1, l, a2);
//cout << "d" << endl;
    result[i][j] = b;
  }
//cout << "d" << endl;
  return result;  
}


matrixd multiplication_matrix_right_huffman_d(matrixd A, matrixd B, int n, int m, int l) {
  matrixd result(n, rowd(m));

  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++) {
      rowd a1(l);
	  rowd a2(l);
      for(int it = 0; it < l; it++) {
		  a1[it] = A[i][it];
		  a2[it] = B[it][j];
	  }
      double b;
//cout << "d" << endl;
    b = multiplication_right_huffman_d(a1, l, a2);
//cout << "d" << endl;
    result[i][j] = b;
  }
//cout << "d" << endl;
  return result;  
}


matrix multiplication_matrix(matrix A, matrix B, int n, int m, int l) { 
  matrix C(SIZE*SIZE, row(SIZE*SIZE));
  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++)
      for(int k = 0; k < l; k++){
        C[i][j] = C[i][j] + (A[i][k]*B[k][j]);
      }
  return C;
}

matrixd multiplication_matrix_d(matrixd A, matrixd B, int n, int m, int l) { 
  matrixd C(SIZE*SIZE, rowd(SIZE*SIZE));

  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++)
      for(int k = 0; k < l; k++){
        C[i][j] = C[i][j] + (A[i][k]*B[k][j]);
      }
  return C;
}


matrix multiplication_wise(matrix A, matrix B, int n, int m) { 
  matrix C(SIZE, row(SIZE));

  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++)
      C[i][j] = A[i][j]*B[i][j];
  return C;
}

matrixd multiplication_wise_d(matrixd A, matrixd B, int n, int m) { 
  matrix C(SIZE, row(SIZE));
  matrixd result(SIZE, rowd(SIZE)); 

  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++){
      C[i][j] = (float)A[i][j]*(float)B[i][j];
      result[i][j] = (double)C[i][j];
  }
  return result;
}

row multiplication(matrix A, int n, int m, row b) { 
  row c(SIZE*SIZE);

  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++){
      c[i] = c[i]+(A[i][j]*b[j]);
    }
  return c;
}

matrix transp(matrix A, int n, int m) { 
  matrix AT(SIZE, row(SIZE));

  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      AT[i][j] = A[j][i];
  return AT;
}

matrixd transp_d(matrixd A, int n, int m) { 
  matrixd AT(SIZE, rowd(SIZE));

  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      AT[i][j] = A[j][i];
  return AT;
}

int main(int argc, char* argv[]) {
	
if(argc != 3){
  cout << "Bad number of arguments \n";
  return 1;
}

int L = 5000; //number of loops
int n_h = atoi(argv[1]); //size of kernel
int n_o = atoi(argv[2]); //size of output
//int m_h = n_h;
//int m_o = n_o; //liczba kolumn wyjscia


 int n_i = n_h + n_o -1; //size of input 
//int m_i = m_h + m_o - 1; //liczba kolumn wejscia

 rowd sd(L);
 
matrix G1(SIZE, row(SIZE)); //matrix G 
matrix G1T(SIZE, row(SIZE)); //matrix G^T
matrixd G1d(SIZE, rowd(SIZE)); //G in double
matrixd G1Td(SIZE, rowd(SIZE)); //G^T in double 
matrix B1T(SIZE, row(SIZE)); //matrix B^T 
matrix B1(SIZE, row(SIZE)); //matrix B 
matrixd B1Td(SIZE, rowd(SIZE)); //matrix B^T in double  
matrixd B1d(SIZE, rowd(SIZE)); //matrix B in double  
matrix A1T(SIZE, row(SIZE)); //matrix A^T  
matrix A1(SIZE, row(SIZE)); //matrix A 
matrixd A1Td(SIZE, rowd(SIZE)); //matrix A^T in double  
matrixd A1d(SIZE, rowd(SIZE)); //matrix A in double  

matrix G(SIZE, row(SIZE)); 
matrix GG(SIZE, row(SIZE));
matrix B(SIZE, row(SIZE));
matrix BB(SIZE, row(SIZE));
matrix A(SIZE, row(SIZE));
matrix AA(SIZE, row(SIZE)); //Winograd convolution
matrix W(SIZE, row(SIZE));
matrix Wh(SIZE, row(SIZE));
matrixd Gd(SIZE, rowd(SIZE)); 
matrixd GGd(SIZE, rowd(SIZE));
matrixd Bd(SIZE, rowd(SIZE));
matrixd BBd(SIZE, rowd(SIZE));
matrixd Ad(SIZE, rowd(SIZE));
matrixd AAd(SIZE, rowd(SIZE)); //Winograd convolution with Huffman in mixed prec
matrixd Wd(SIZE, rowd(SIZE));
matrix Gh(SIZE, row(SIZE)); 
matrix GGh(SIZE, row(SIZE));
matrix Bh(SIZE, row(SIZE));
matrix BBh(SIZE, row(SIZE));
matrix Ah(SIZE, row(SIZE));
matrix AAh(SIZE, row(SIZE)); //Winograd convolution with Huffman

matrix H(SIZE, row(SIZE)); //kernel matrix 
matrix X(SIZE, row(SIZE)); //input matrix 
matrixd Hd(SIZE, rowd(SIZE)); //kernel matrix in double 
matrixd Xd(SIZE, rowd(SIZE)); //input matrix in double
matrix S(SIZE, row(SIZE)); //direct convolution 
matrixd D(SIZE, rowd(SIZE)); //direct convolution in double

//double error; //blad rozwiazania
//double L2_error = 0;
double error_W_all = 0;
double error_Wh_all = 0;
double error_W_d_all = 0;
double error_d_all = 0;

ifstream fB1T("BTMat.txt");
  for(int i = 0; i < n_i; i++) 
    for(int j = 0; j < n_i; j++) {
      long long num;
      long long den;
      fB1T >> num >> den;
      B1T[i][j] = (float)num/(float)den;
        }
  fB1T.close();

for(int i = 0; i < n_i; i++)
  for(int j = 0; j < n_i; j++)
    B1Td[i][j] = (double)B1T[i][j];

//ifstream fB2T("BTMat.txt");
//  for(int i = 0; i < n_i; i++) 
//    for(int j = 0; j < n_i; j++) {
//      long long num;
//      long long den;
//      fB2T >> num >> den;
//      B2T[i][j] = (double)num/(double)den;
//        }
//  fB2T.close();

ifstream fA1T("ATMat.txt");
  for(int i = 0; i < n_o; i++) 
    for(int j = 0; j < n_i; j++) {
      long long num;
      long long den;
      fA1T >> num >> den;
      A1T[i][j] = (float)num/(float)den;
        }
  fA1T.close();

for(int i = 0; i < n_o; i++)
  for(int j = 0; j < n_i; j++)
    A1Td[i][j] = (double)A1T[i][j];

//ifstream fA2T("ATMat.txt");
//  for(int i = 0; i < n_o; i++) 
//    for(int j = 0; j < n_i; j++) {
//      long long num;
//      long long den;
//      fA2T >> num >> den;
//      A2T[i][j] = (double)num/(double)den;
//        }
//  fA2T.close();

ifstream fG1("GMat.txt");
  for(int i = 0; i < n_i; i++) 
    for(int j = 0; j < n_h; j++) {
      long long num;
      long long den;
      fG1 >> num >> den;
      G1[i][j] = (float)num/(float)den;
        }
  fG1.close();

for(int i = 0; i < n_i; i++)
  for(int j = 0; j < n_h; j++)
    G1d[i][j] = (double)G1[i][j];

G1T = transp(G1, n_i, n_h);
G1Td = transp_d(G1d, n_i, n_h);

B1 = transp(B1T, n_i, n_i);
B1d = transp_d(B1Td, n_i, n_i);

A1 = transp(A1T, n_o, n_i);
A1d = transp_d(A1Td, n_o, n_i);


//ifstream fG2("GMat.txt");
//  for(int i = 0; i < n_i; i++) 
//    for(int j = 0; j < n_h; j++) {
//      long long num;
//      long long den;
//      fG2 >> num >> den;
//      G2[i][j] = (double)num/(double)den;
//        }
//  fG2.close();


for(int it = 0; it < L; it++) {
  for(int i = 0; i < n_o; i++)
	  for(int j = 0; j < n_o; j++){
      S[i][j] = 0;
      D[i][j] = 0;
  }
  
std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<float> dis(-1,1);

for(int i = 0; i < n_h; i++)
  for(int j = 0; j < n_h; j++){
    H[i][j] =  dis(gen);
    Hd[i][j] = (double)H[i][j];
}

for(int i = 0; i < n_i; i++)
  for(int j = 0; j < n_i; j++){ 
    X[i][j] = dis(gen);
    Xd[i][j] = (double)X[i][j];
}



G = multiplication_matrix(G1, H, n_i, n_h, n_h);
Gh = multiplication_matrix_left_huffman(G1, H, n_i, n_h, n_h);
Gd = multiplication_matrix_left_huffman_d(G1d, Hd, n_i, n_h, n_h);

GG = multiplication_matrix(G, G1T, n_i, n_i, n_h);
GGh = multiplication_matrix_right_huffman(Gh, G1T, n_i, n_i, n_h);
GGd = multiplication_matrix_right_huffman_d(Gd, G1Td, n_i, n_i, n_h);



B = multiplication_matrix(B1T, X, n_i, n_i, n_i);
Bh = multiplication_matrix_left_huffman(B1T, X, n_i, n_i, n_i);
Bd = multiplication_matrix_left_huffman_d(B1Td, Xd, n_i, n_i, n_i);

BB = multiplication_matrix(B, B1, n_i, n_i, n_i);
BBh = multiplication_matrix_right_huffman(Bh, B1, n_i, n_i, n_i);
BBd = multiplication_matrix_right_huffman_d(Bd, B1d, n_i, n_i, n_i);

W = multiplication_wise(GG, BB, n_i, n_i);
Wh= multiplication_wise(GGh, BBh, n_i, n_i);
Wd = multiplication_wise_d(GGd, BBd, n_i, n_i);


A = multiplication_matrix(A1T, W, n_o, n_i, n_i);
Ah = multiplication_matrix_left_huffman(A1T, Wh, n_o, n_i, n_i);
Ad = multiplication_matrix_left_huffman_d(A1Td, Wd, n_o, n_i, n_i);

AA = multiplication_matrix(A, A1, n_o, n_o, n_i);
AAh = multiplication_matrix_right_huffman(Ah, A1, n_o, n_o, n_i);
AAd = multiplication_matrix_right_huffman_d(Ad, A1d, n_o, n_o, n_i);
//cout << "Winograd:" << '\n';
//print_matrix(AA, n_o, m_o);
//cout << '\n';

for(int i = 0; i < n_o; i++)
  for(int j = 0; j < n_o; j++)
    for(int k = 0; k < n_h; k++)
      for(int l = 0; l < n_h; l++){
         S[i][j] = S[i][j] + (H[k][l]*X[k+i][l+j]);
      }
//cout << "Direct convolution:" << '\n';
//print_matrix(S, n_o, m_o);

for(int i = 0; i < n_o; i++)
  for(int j = 0; j < n_o; j++)
    for(int k = 0; k < n_h; k++)
      for(int l = 0; l < n_h; l++){
         D[i][j] = D[i][j] + (Hd[k][l]*Xd[k+i][l+j]);
      }
//cout << "Direct convolution ground true:" << '\n';
//for(int i = 0; i < n_o; i++){
//  for(int j = 0; j < m_o; j++)
//    cout << std::fixed << std::setprecision(12) << D[i][j];
//  cout << '\n';
//}

double error_W_g = 0; //Winograd 
double error_Wh_g = 0; //Winograd Huffman 
double error_d_g = 0; //winograd mixwd Huffman
double error_W_d = 0; //direct

for(int i = 0; i < n_o; i++)
  for(int j = 0; j < n_o; j++) {
    error_W_g = error_W_g + abs(AA[i][j] - D[i][j]);
    error_Wh_g = error_Wh_g + abs(AAh[i][j] - D[i][j]);
    error_W_d = error_W_d + abs(AAd[i][j] - D[i][j]);
    error_d_g = error_d_g + abs(S[i][j] - D[i][j]);
}
// sd[it]=error_W_d;
//error_W_g = sqrt(error_W_g);
//error_Wh_g = sqrt(error_Wh_g);
//error_W_d = sqrt(error_W_d);
//error_d_g = sqrt(error_d_g);

  error_W_all = error_W_all + error_W_g;
  error_Wh_all = error_Wh_all + error_Wh_g;
  error_W_d_all = error_W_d_all + error_W_d;
  error_d_all = error_d_all + error_d_g;
}

 
error_W_all = error_W_all/L;
error_Wh_all = error_Wh_all/L;
error_W_d_all = error_W_d_all/L;
error_d_all = error_d_all/L;
// double Ss=0;
// for(int i = 0; i <L;i++)
//   Ss=Ss+pow(sd[i]-error_W_d_all,2);
// Ss=Ss/(L-1);
// Ss=sqrt(Ss);
//double error_W_output = error_W_all/(double)n_o;
 cout << std::fixed << std::setprecision(12) << " Winograd mixed Huffman " << error_W_d_all 
 << " Winograd: " << error_W_all << " Winograd huffman: " << error_Wh_all <<" Direct: " << error_d_all << endl;
//" Winograd: " << std::fixed << std::setprecision(12) << error_W_all << " Winograd Huffman " << error_Wh_all << endl << " Double+float " << error_W_d_all << " Direct: " << error_d_all << endl;
// << " Winograd v direct: " << error_W_d << '\n';
return 0;
}
